// ==UserScript==
// @name        webnovel.com no comments
// @namespace   https://github.com/adminfromhell/webnovel-no-comments
// @version     0.1.0
// @description Remove comments from webnovel browser pages
// @author      adminfromhell
// @run-at      document-end
// @match       http://webnovel.com/book/*
// @match       https://webnovel.com/book/*
// @match       http://www.webnovel.com/book/*
// @match       https://www.webnovel.com/book/*
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==

var $ = window.jQuery;

function remove_comments() {
    $('.j_bottom_comment_area').remove()
}

function run() {
    remove_comments()
    setTimeout(run, 1000)
}

run()
